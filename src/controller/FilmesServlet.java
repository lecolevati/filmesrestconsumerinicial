package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.Filme;
import model.Genero;
import persistence.FilmesDao;
import persistence.IFilmesDao;

@WebServlet("/filmes")
public class FilmesServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public FilmesServlet() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	protected void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String acao = request.getParameter("acao");
		Filme f = new Filme();
		if (request.getParameter("idFilme").isEmpty()) {
			f.setIdFilme(0);
		} else {
			f.setIdFilme(Integer.parseInt(request.getParameter("idFilme")));
		}

		f.setNomeFilme(request.getParameter("nomeFilme"));
		Genero g = new Genero();
		if (request.getParameter("codGenero").isEmpty()) {
			g.setCodGenero(0);
		} else {
			g.setCodGenero(Integer.parseInt(request.getParameter("codGenero")));
		}
		g.setTipoGenero(request.getParameter("tipoGenero"));
		f.setGeneroFilme(g);
		String erro = "";

		try {
			if (f.getIdFilme() >= 0) {
				IFilmesDao fDao = new FilmesDao();

				if (acao.equals("insert")) {
					String retorno = insereFilme(f, fDao);
					request.setAttribute("retorno", retorno);
				}
				if (acao.equals("update")) {
					String retorno = atualizaFilme(f, fDao);
					request.setAttribute("retorno", retorno);
				}
				if (acao.equals("delete")) {
					String retorno = excluiFilme(f, fDao);
					request.setAttribute("retorno", retorno);
				}
				if (acao.equals("getFilme")) {
					f = buscaFilme(f, fDao);
					request.setAttribute("filme", f);
				}
				if (acao.equals("getAllFilmes")) {
					List<Filme> lista = buscaTodosFilmes(fDao);
					request.setAttribute("lista", lista);
				}
				if (acao.equals("getFilmeId")) {
					List<Filme> lista = buscaFilmeId(f, fDao);
					request.setAttribute("lista", lista);
				}
			}
		} catch (IOException e) {
			erro = e.getMessage();
		} finally {
			request.setAttribute("erro", erro);
			RequestDispatcher rd = request.getRequestDispatcher("index.jsp");
			rd.forward(request, response);
		}

	}

	private String insereFilme(Filme f, IFilmesDao fDao) throws IOException {
		return fDao.adicionaFilme(f);
	}

	private String atualizaFilme(Filme f, IFilmesDao fDao) throws IOException {
		return fDao.atualizaFilme(f);
	}

	private String excluiFilme(Filme f, IFilmesDao fDao) throws IOException {
		if (fDao.excluiFilme(f)) {
			return "Exclu�do com sucesso";
		} else {
			return "Erro ao tentar excluir";
		}
	}

	private Filme buscaFilme(Filme f, IFilmesDao fDao) throws IOException {
		return fDao.consultaFilmeporIdGenero(f);
	}

	private List<Filme> buscaTodosFilmes(IFilmesDao fDao) throws IOException {
		Filme filme = new Filme();
		return fDao.consultaFilmes(filme);

	}

	private List<Filme> buscaFilmeId(Filme f, IFilmesDao fDao) throws IOException {
		return fDao.consultaFilmes(f);
	}

}
