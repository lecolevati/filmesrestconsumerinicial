package persistence;

import java.io.IOException;
import java.util.List;

import model.Filme;

public interface IFilmesDao {

	public List<Filme> consultaFilmes(Filme f) throws IOException;
	public Filme consultaFilmeporIdGenero(Filme f) throws IOException;
	public String adicionaFilme(Filme f) throws IOException;
	public boolean excluiFilme(Filme f) throws IOException;
	public String atualizaFilme(Filme f) throws IOException;
	
}
